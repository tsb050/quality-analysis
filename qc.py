from tkinter import *
from tkinter import filedialog
import numpy as np
import os
from PIL import ImageTk,Image  
from PIL import Image
import PIL
import glob
import xarray as xr
import yaml

import datetime
import dateutil.parser
from geopy.distance import distance
import os
import os.path

from utils import verify_displacement
from utils import plot_ocean_data

def top():
    windowtop = Tk()
    windowtop.title("Welcome")
    windowtop.geometry('750x400')
    Elbl1 = Label(windowtop, text="Enter thy name ")
    Elbl1.grid(column=1, row=4, pady=25)
    text1 = Text(windowtop, height=2, width=30)
    text1.grid(column=1, row=7)
    def front():
        windowtop.withdraw()
        window = Toplevel(windowtop)
        window.title("Welcome ")
        window.geometry('750x400')
        lbl = Label(window, text="Drifter data analysis for {}".format(text1.get("1.0",END)), pady=10)
        lbl.grid(column=1, row=0)
        Elbl = Label(window, text="Browse ")
        Elbl.grid(column=0, row=4)
        def new_window():
            window.withdraw()
            def OpenFile():
                root = Tk()
                root.filename =  filedialog.askopenfilename(initialdir = os.getcwd(),title = "Select file",filetypes = (("netcdf files","*.nc"),("all files","*.*")))
                root.destroy()
                return root.filename
            new_file = OpenFile()
            newwin = Toplevel(window)
            newwin.geometry('750x400')
            displayT = Text(newwin, height=20, width=70)
            displayT.grid(column=1, row=4)
            btn = Button(newwin, text="Quit", command=windowtop.destroy)
            btn.grid(padx=10, pady=45)
            def image_display():
                master = Toplevel()
                basewidth = 500
                img = Image.open('salishseacast_ar_2017041100_P10D.png')
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img.save('reno.png')
                master.geometry("550x525")
                label = Label(master)
                label.img = PhotoImage(file='reno.png')
                label.config(image=label.img)
                label.pack()
                master.mainloop()
            btn = Button(newwin, text="Display", command=image_display)
            btn.grid(padx=40, pady=45)
            displayT.insert(END, new_file)
            verify(new_file, displayT)
        btn = Button(window, height=2, width=80, text="Quality Check", command=new_window)
        btn.grid(column=1, row=4)
        #windowtop.destry()
    btn = Button(windowtop, text="Enter", command=front)
    btn.grid(padx=100, pady=25)
    windowtop.mainloop()

def verify(file_name, displayT):
    displayT.insert(END, "\n" + 'Hmmmm a netcdf file')
    print(type(file_name))
    probe_drifters = []
    try:
        dataset = xr.open_dataset(file_name)
        #displayT.insert(END, "\n" + dataset)
        if 'time' not in dataset.coords:
            print('No time coordinate...')
            displayT.insert(END, "\n" + 'No time coordinate...')
        else :
            def dataset_time_probe(qc_list):
                print('Checking for drifter time accuracy...')
                # Check dataset date accuracy, this determines if time is sorted in drifter data
                if(dataset.time.values.min() != dataset.time.values[0] or 
                    dataset.time.values.max() != dataset.time.values[-1]):
                    print('Inaccurate/unsorted drifter time for {}'.format(file_name))
                    displayT.insert(END, "\n" + 'Inaccurate/unsorted drifter time for {}'.format(file_name))
                    drifters = dict(
                        message = "Inaccurate drifter time for drifter {}".format(file_name),
                        filename = file_name
                    )
                    qc_list.append(drifters)
                else:
                    print('Drifter time checked for {}...'.format(file_name))
                    displayT.insert(END, "\n" + 'Drifter time checked for {}...'.format(file_name))
                    drifters = dict(
                        message = 'Drifter time checked for {}...'.format(file_name)
                    )
                    qc_list.append(drifters)
                return qc_list
            time_result = []
            disp_result = []
            print(dataset_time_probe(time_result))
            print(verify_displacement(disp_result, dataset, file_name, displayT))
            pic_file=plot_ocean_data(dataset)

    except OSError:
        # Some drifter files failed to open. Probably bad files
        print('Drifter {} failed to open...'.format(file_name))
        displayT.insert(END, "\n" + 'Drifter {} failed to open...'.format(file_name))
        drifters = dict(
            message="Drifter {} failed to open".format(file_name),
            filename = file_name
            )
        probe_drifters.append(drifters)

    now = datetime.datetime.utcnow()
    metadata = dict(updated=now.isoformat(), Failed_drifters=probe_drifters, 
                    Time_Check=time_result, Displacement_Check=disp_result)
    print('Dumping drifter verification information...')
    displayT.insert(END, "\n" + 'Dumping drifter verification information...')
    with open('verify_drifter.yaml', 'w') as f:
        yaml.dump(metadata, f, default_flow_style=False)
        
def main():
    top()

if __name__ == '__main__':
    main()