import numpy as np
import os
import glob
import xarray as xr
import yaml

import datetime
import dateutil.parser
from geopy.distance import vincenty
import os
import os.path


def verify(file_name):
    try:
        dataset = xr.open_dataset(file_name)
        print(dataset)
        if 'time' not in dataset.coords:
            print('No time coordinate...')
        else :
            def dataset_time_probe(qc_list):
                print('Checking for drifter time accuracy...')
                # Check dataset date accuracy, this determines if time is sorted in drifter data
                if(dataset.time.values.min() != dataset.time.values[0] or 
                    dataset.time.values.max() != dataset.time.values[-1]):
                    print('Inaccurate/unsorted drifter time for {}'.format(file_name))
                    drifters = dict(
                        message = "Inaccurate drifter time for drifter {}".format(file_name),
                        filename = file_name
                    )
                    qc_list.append(drifters)
                else:
                    print('Drifter time checked for {}...'.format(file_name))
                    drifters = dict(
                        message = 'Drifter time checked for {}...'.format(file_name)
                    )
                    qc_list.append(drifters)
                return qc_list

            def verify_displacement(qc_list):
                # Calculate displacement of drifters 
                displacement = vincenty((dataset.mod_lat[0][0], dataset.mod_lon[0][0]),
                                (dataset.mod_lat[0][-1], dataset.mod_lon[0][-1])).meters
                print(displacement/1000)
                km_disp = displacement/1000

                # If displacement is less than 0.2. I'm assuming the drifter did not move 
                # stuck on a coastline
                if(km_disp <= 0.2):
                    print('Drifter {} did not move or is stuck'.format(file_name))
                    drifters = dict(
                        message="Drifter {} did not move or is stuck".format(file_name),
                        filename = file_name
                    )
                    #probe_drifters.append(drifters)
                    qc_list.append(drifters)
                else:
                    print('Drifter displacement checked for {}...'.format(file_name))
                    drifters = dict(
                        message = 'Drifter displacement checked for {}...'.format(file_name)
                    )
                    qc_list.append(drifters)
                dataset.close()
                return qc_list

            time_result = []
            disp_result = []
            probe_drifters = []
            print(dataset_time_probe(time_result))
            print(verify_displacement(disp_result))

    except OSError:
        # Some drifter files failed to open. Probably bad files
        print('Drifter {} failed to open...'.format(file_name))
        drifters = dict(
            message="Drifter {} failed to open".format(file_name),
            filename = file_name
            )
        probe_drifters.append(drifters)

    now = datetime.datetime.utcnow()
    metadata = dict(updated=now.isoformat(), Failed_drifters=probe_drifters, 
                    Time_Check=time_result, Displacement_Check=disp_result)
    print('Dumping drifter verification information...')     
    with open('verify_drifter.yaml', 'w') as f:
        yaml.dump(metadata, f, default_flow_style=False)


def domain_check(mesh_file, start_date, qc_list):
    lat, lon = get_drifter_location(date, dataset)
    # Is the drifter in the domain?
    ds = xr.open_dataset(mesh_file)
    mesh_lon_min = np.min(ds[lon_var].values[np.nonzero(ds[lon_var].values)])
    mesh_lon_max = np.max(ds[lon_var].values[np.nonzero(ds[lon_var].values)])
    mesh_lat_min = np.min(ds[lat_var].values[np.nonzero(ds[lat_var].values)])
    mesh_lat_max = np.max(ds[lat_var].values[np.nonzero(ds[lat_var].values)])
    ds.close()

    def write_to_dict():
        print('Drifter is outside the specified domain')
        drifters = dict(
            message='Drifter {} is outside the specified domain'.format(file_name),
            filename = file_name)
        qc_list.append(drifters)
        return drifters
    # Probe
    if(lon < mesh_lon_min):
        drifters = write_to_dict()
    elif(lon > mesh_lon_max):
        drifters = write_to_dict()
    elif(lat < mesh_lat_min):
        drifters = write_to_dict()
    elif(lat > mesh_lat_max):
        drifters = write_to_dict()
    return qc_list

def depth_check(qc_list):
    if 'drogue' not in dataset.attrs:
        print('No depth attribute in dataset for {}/ \n Attribute might be named differently'.format(file_name))
    else:
        if(self.drifter_depth != dataset.drogue):
            print('Depth specified by user is different from drifter depth')
            drifters = dict(
                message='Depth specified by user is different from drifter {} depth'.format(file_name),
                filename = file_name)
            qc_list.append(drifters)
    return qc_list

def main():
    verify('salishseacast_ar_2017041100_P10D.nc')

if __name__ == '__main__':
    main()