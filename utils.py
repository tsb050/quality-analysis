from geopy.distance import distance
from tkinter import *
from collections import namedtuple
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.pylab import cm
from mpl_toolkits.basemap import Basemap
from collections import OrderedDict

import glob
import math
import numpy as np
import os
from os.path import join as joinpath
import seawater as sw

def verify_displacement(qc_list, dataset, file_name, displayT):
    # Calculate displacement of drifters
    displacement = distance((dataset.mod_lat[0][0], dataset.mod_lon[0][0]),
                            (dataset.mod_lat[0][-1], dataset.mod_lon[0][-1])).meters
    print(displacement/1000)
    km_disp = displacement/1000

    # If displacement is less than 0.2. I'm assuming the drifter did not move
    # stuck on a coastline
    if(km_disp <= 0.2):
        print('Drifter {} did not move or is stuck'.format(file_name))
        displayT.insert(
            END, "\n" + 'Drifter {} did not move or is stuck'.format(file_name))
        drifters = dict(
            message="Drifter {} did not move or is stuck".format(file_name),
            filename=file_name
        )
        qc_list.append(drifters)
    else:
        print('Drifter displacement checked for {}...'.format(file_name))
        displayT.insert(
            END, "\n" + 'Drifter displacement checked for {}...'.format(file_name))
        drifters = dict(
            message='Drifter displacement checked for {}...'.format(file_name)
        )
        qc_list.append(drifters)
    return qc_list


LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                    ('lat_min', 'lat_max','lon_min', 'lon_max'))

def plot_ocean_data(ds, etopo_file=None, output_file=None):
    """Plot the modelled ocean data
   
    Parameters
    ----------
    file_name : file
         file containing the computed trajecjory longitude and latitude 
        values.
    etopo_file : str or file-like object, optional
        Path to bathymetry file.
    output_file : str or file-like object, optional
        File to save ocean data plots to. If not provided, no plot file is saved.
    """
    #ds = xr.open_dataset(file_name)
    plt.style.use('seaborn-talk')
    fig, ax = plt.subplots(1,1,figsize=(20,15))
    cmap = plt.cm.get_cmap('copper')
    plt.title('Drift map of {}: duration = {} Hours'.format
            (ds.mod_run_name, ds.mod_nb_output))

    # constants for bounding box area extension
    buffer_lon = 1.50
    buffer_lat = 1.00
    buffer_lat_max = 1.0
    buffer_lon_min = 2.10
    
    initial_bbox = LatLonBoundingBox(lon_min = ds.mod_lon.min()-2, lat_min = ds.mod_lat.min()-2, 
                                        lon_max = ds.mod_lon.max()+2, lat_max = ds.mod_lat.max()+2)
   
    dlat = initial_bbox.lat_max - initial_bbox.lat_min
    dlon = initial_bbox.lon_max - initial_bbox.lon_min
    
    basemap_plot=Basemap(projection='merc', resolution='l', llcrnrlon=initial_bbox.lon_min, 
                         llcrnrlat=initial_bbox.lat_min, urcrnrlon=initial_bbox.lon_max, 
                         urcrnrlat=initial_bbox.lat_max)
    
    if etopo_file is not None:
        plot_bathymetry(etopo_file,basemap_plot, initial_bbox)

    # Calculate maximum value of color bar
    dispmax = np.nanmax(ds.mod_disp.values)
    if dispmax/1000. > 400:
        vmax = 400
        vmin = 50
    else:
        vmax = round_up_base10(dispmax/1000.)
        vmin = 0
    for md in range(0, len(ds.model_run)):
        traj_value_x, traj_value_y = basemap_plot(ds.mod_lon[md].values,
                                                  ds.mod_lat[md].values)
        basemap_plot.plot(traj_value_x, traj_value_y,
                          '-', linewidth = 0.4, color= 'k')
        mesh = basemap_plot.scatter(traj_value_x, traj_value_y,
                                    c=ds.mod_disp[md].values/1000,
                                    cmap=cmap, marker='o', s=6,
                                    vmin=vmin, vmax=vmax)
        basemap_plot.plot(traj_value_x[0], traj_value_y[0],
                          '.', color='k', markersize = 3)
        basemap_plot.plot(traj_value_x[-1], traj_value_y[-1],
                          '.', color='sandybrown', markersize = 3)
        if md == 0:
            c_bar =basemap_plot.colorbar(mesh,location='bottom',pad="8%")
            c_bar.set_label('Displacement [km]')
    basemap_plot.drawmapboundary(fill_color='#A6CAE0', linewidth=0)
    
    # draw coastlines, country boundaries, fill continents.
    basemap_plot.fillcontinents(color='lightgrey', alpha=0.7,
                                lake_color='grey')
    basemap_plot.drawcoastlines(linewidth=0.8, color="black")
    
    # inputs for scale (km) on the map
    scale_lon_min = initial_bbox.lon_max - 3.8
    scale_lon_max = initial_bbox.lon_max - 2.5
    scale_lat_min = initial_bbox.lat_max - 1.8
    scale_lat_max = initial_bbox.lat_max - 1.20
    length = round(ds.mod_disp.values.max()//1000, -3)
    basemap_plot.drawmapscale(scale_lon_min,
                              scale_lat_min,
                              scale_lon_max,
                              scale_lat_max, 
                              vmax, fontsize=12)
        
    # parallels and meridiands for the maps
    meridian_dlat = dlat/3
    meridian_dlon = dlon/3
    parallels = np.arange(initial_bbox.lat_min,
                          initial_bbox.lat_max,
                          meridian_dlat)
    basemap_plot.drawparallels(parallels, labels=[1, 0, 0, 0],
                               fontsize=14, color='0.5', 
                               linewidth=0.5, fmt='%0.2f')
    meridians = np.arange(initial_bbox.lon_min,
                          initial_bbox.lon_max,
                          meridian_dlon)
    basemap_plot.drawmeridians(meridians,
                               labels=[0, 0, 0, 1],
                               fontsize = 14,
                               color='0.5', 
                               linewidth=0.5, fmt='%0.2f')
    if output_file is None:
        output_file = '{}.png'.format(ds.mod_run_name)
    fig.savefig(output_file, bbox_inches='tight', dpi=300, 
                papertype='letter', orientation='portrait')
    plt.close(fig)
    return output_file

def round_up_base10(number):
    """ Routine for clean rounding up based on powers of 10
        eg. 1475 rounds to 2000
        15 rounds to 20
        0.01343453 rounds to 0.02

        arg number: float
            number to be rounded
    """
    if number != 0:
        power = np.floor(np.log10(np.abs(number)))
        result = 10**power*np.ceil(number/10**power)
        if number < 0:
           result = -result
    else:
        result = 0
    return result
